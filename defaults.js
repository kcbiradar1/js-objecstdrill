const _= require("underscore")

module.exports = function defaults(testObjects) {
    return _.defaults(testObjects);
}