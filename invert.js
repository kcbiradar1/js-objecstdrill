const _= require("underscore")

module.exports = function invert(testObjects) {
    return _.invert(testObjects);
}